export interface Weather {
    resourceUrl?: string,
    resource: WeatherData,

}

interface WeatherData{
    id?: string,
    temperature: number,
    humidity: number,
    pressure: number,
    subscriptions?: string[]
}