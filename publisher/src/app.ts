import express, { Application } from 'express';
import routerWeather from './routes/api.routes';

export class App {
    app: Application;
    constructor() {
        this.app = express();
    }
    setRoutes() {
        this.app.use('/api/v1/', routerWeather);
    }
    setMiddlewares() {
        this.app.use(express.json());
    }
    async start(port: number) {
        await this.app.listen(port);
        console.log(`Server running on Port ${port}`);
    }
}