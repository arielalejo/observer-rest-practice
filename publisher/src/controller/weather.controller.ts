import { Request, Response } from 'express';
import { WeatherService } from '../services/weatherService';
import { Weather } from '../models/wheather.model';

const weatherService = WeatherService.Instance;

export class WeatherController {

    addWeatherData(req: Request, res: Response) {
        const weatherBody = <Weather>req.body;
        const createdWeather = weatherService.addWeatherData(weatherBody);
        res.json(createdWeather);
    }

    addSubscriber(req: Request, res: Response){
        const { subscriber } = req.body;
        const createdSubscriber = weatherService.addSubscriber(subscriber);
        res.json(createdSubscriber);
    }
}
