import { Router } from 'express';
import { WeatherController } from '../controller/weather.controller';

const routerAPI = Router();
const weatherController = new WeatherController();

routerAPI.post('/subscriptions', weatherController.addSubscriber);
routerAPI.post('/weather', weatherController.addWeatherData);

export default routerAPI;
