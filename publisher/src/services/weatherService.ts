import { Weather } from '../models/wheather.model';
import { DAOFacade } from '../dao/weather.facade.dao';
const fetch = require('node-fetch');


export class WeatherService {
    private static instance : WeatherService;
    private constructor(){  }
    static get Instance(): WeatherService{
        if (!this.instance) {
            this.instance =  new WeatherService();
        }
        return this.instance;
    }
    addSubscriber(subscriber: string){
        const addedSubscriber = DAOFacade.WeatherDAO.addSubscriber(subscriber);
        return addedSubscriber;
    }

    addWeatherData(weather: Weather) {        
        const addedWeather = DAOFacade.WeatherDAO.addNotification(weather);
        if (addedWeather.resource.subscriptions) {
            fetch(`${addedWeather.resource.subscriptions[0]}/notifications`, 
                {   method: 'POST', 
                    body: JSON.stringify(addedWeather), 
                    headers: { 'Content-Type': 'application/json' } })
                .then((response: any ) => {})
                .catch((error:any )=> console.log(error)) ;
        }
        return addedWeather;
    }


}