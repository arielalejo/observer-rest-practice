import { WeatherDAO } from './weather.dao';

export class DAOFacade{
    static readonly WeatherDAO = new WeatherDAO();
    private constructor() {}
}