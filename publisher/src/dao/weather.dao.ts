import { v4 as uuidv4 } from 'uuid';
import { Weather } from '../models/wheather.model';
import { WeatherCollection, Subscriptions } from './serviceData';

export class WeatherDAO{
    
    addSubscriber(subscriber: string){
        Subscriptions.push(subscriber);
        return Subscriptions;
    }

    addNotification(weather: Weather) {
        weather.resourceUrl = 'http://localhost:3001/api/v1/weather';
        weather.resource.id = uuidv4();
        weather.resource.subscriptions = Subscriptions;
        WeatherCollection.push(weather);
        return weather;
    }

}
