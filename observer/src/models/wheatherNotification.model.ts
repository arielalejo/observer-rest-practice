export interface WeatherNotification {
    resourceUrl?: string,
    resource?: Weather,

}

interface Weather{
    id?: string,
    temperature: number,
    humidity: number,
    pressure: number
    suscriptions?: string[]

}