import { Router } from 'express';
import { NotificationsController } from '../controller/notifications.controller';

const routerAPI = Router();
const notificationsController = new NotificationsController();

routerAPI.post('/notifications', notificationsController.addNotification);
routerAPI.get('/notifications', notificationsController.getNotifications);
export default routerAPI;
