import { App } from './app';


const app = new App();
app.setMiddlewares();
app.setRoutes();
app.start(3002);