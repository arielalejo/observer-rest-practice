import { WeatherNotification } from '../models/wheatherNotification.model';
import { DAOFacade } from '../dao/notifications.facade.dao';


export class NotificationService {
    private static instance : NotificationService;
    private constructor(){  }
    static get Instance(): NotificationService{
        if (!this.instance) {
            this.instance =  new NotificationService();
        }
        return this.instance;
    }

    addNotification(notification: WeatherNotification) {        
        const addedNotification = DAOFacade.NotificationsDAO.addNotification(notification);
        return addedNotification;
    }
    getNotifications(){
        const notifications = DAOFacade.NotificationsDAO.getNotifications();
        return notifications;
    }

}