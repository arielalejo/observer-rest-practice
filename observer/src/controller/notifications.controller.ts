import { Request, Response } from 'express';
import { NotificationService } from '../services/notificationService';
import { WeatherNotification } from '../models/wheatherNotification.model';

const notificationService = NotificationService.Instance;

export class NotificationsController {

    addNotification(req: Request, res: Response) {
        const notificationBody = req.body; 
        const createdNotification = notificationService.addNotification(notificationBody);
        res.json(createdNotification);
    }
    getNotifications (req: Request, res: Response){
        const notifications = notificationService.getNotifications();
        res.json(notifications);
    } 
}
