import { NotificationsDAO } from './notifications.dao';

export class DAOFacade{
    static readonly NotificationsDAO = new NotificationsDAO();
    private constructor() {}
}