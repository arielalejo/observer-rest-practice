import { v4 as uuidv4 } from 'uuid';
import { WeatherNotification } from '../models/wheatherNotification.model';
import { Notifications } from './serviceData';

export class NotificationsDAO{
    
    addNotification(notification: WeatherNotification) {
        Notifications.push(notification);
        return notification;
    }
    getNotifications(){
        return Notifications;
    }

}

